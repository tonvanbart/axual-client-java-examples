# Axual Example Projects

This example repo shows a typical use of the different types of clients available in the Axual Platform.
All examples use the same usecase and resources, making it easier to compare
them.

## Overall stream design
There are five streams used in the application.

- avro-applicationlog
A stream for application log messages
	- Key Avro schema: 
		<br/>io.axual.client.example.schema.Application
	- Value Avro schema: 
		<br/>io.axual.client.example.schema.ApplicationLogEvent
- avro-log-alert-setting
    - Key Avro schema: 
      		<br/>io.axual.client.example.schema.Application
    - Value Avro schema: 
      		<br/>io.axual.client.example.schema.ApplicationLogAlertSetting	
- avro-log-alert-setting-store-changelog
    - Key Avro schema: 
      		<br/>io.axual.client.example.schema.Application
    - Value Avro schema: 
      		<br/>io.axual.client.example.schema.ApplicationLogAlertSetting	
- avro-log-alert
    - Key Avro schema: 
      		<br/>io.axual.client.example.schema.Application
    - Value Avro schema: 
      		<br/>io.axual.client.example.schema.ApplicationLogAlert
- string-applicationlog
    - Key Avro schema: 
      		<br/>N/A (null)
    - Value Avro schema: 
      		<br/>N/A (null)


## Example Applications

### Goals
For the examples several goals are identified

1. Produce and Consume events
2. Produce and Consume events using Axual Client Proxy
3. Make use of Axual Streams.
4. Start a custom Axual Platform Test standalone, using custom Server and Client Certificates

### Client types
The examples use several client types

1. Axual Client\
The high level client for event processing.
2. Axual Client Proxy\
The low level client based on Kafka interfaces and programming model
3. Axual Stream\
It provides a wrapper around Kafka Streams.

### Project Structure
* axual-client-examples\
Contains examples using the Axual Client:
  * axual-client-avro-consumer\
  Consumes messages from the `avro-applicationlog` stream
  * axual-client-avro-log-alert-consumer\
  Consumes messages from the `avro-log-alert` stream
  * axual-client-string-consumer\
  Consumes messages from the `string-applicationlog` stream
  * axual-client-string-producer\
  Produces 10 messages from the `string-applicationlog` stream
  * axual-client-avro-log-alert-setting-producer\
  Produces 10 messages to the `avro-log-alert-setting` stream
  * axual-client-avro-producer\
  Produces 10 messages to the `avro-applicationlog` stream
  * spring-example
  Spring Bean configuration to produce 10 messages to the 
  `string-applicationlog` stream
* axual-client-proxy-examples\
Contains examples using the Axual Client Proxy:
  * axual-client-proxy-avro-consumer\
  Consumes messages from the `avro-applicationlog` stream
  * axual-client-proxy-avro-producer\
  Produces 10 messages to the `avro-applicationlog` stream
  * axual-client-proxy-string-consumer\
  Consumes messages from the `string-applicationlog` stream
  * axual-client-proxy-string-producer\
  Produces 10 messages to the `string-applicationlog` stream
* axual-client-stream-examples\
Contains examples using the Axual Streams:
  * axual-client-stream-avro-to-string
    * Consumes messages from the `avro-applicationlog` stream.
    * Filters the messages based on the application log level.
    * Converts the messages to String format and publishes to `string-applicationlog` stream.
    
    To produce message to `avro-applicationlog` stream, start the `axual-client-avro-producer` app. 
    <br/>To consume the messages from `string-applicationlog` stream, start the `axual-client-string-consumer` app. 
  * axual-client-stream-join\
  `Note: Before using the join, make sure to create intermediate chnagelog stream name as {applicationId}_{storeName}_changelog.
            The example-standalone project creates this intermediate stream, as it already mentioned in the Streams enum (under example-common project).`
    * Consumes messages from `avro-applicationlog` stream.
    * Joins messages by `log-alert-setting` stream.
    * Filters the messages on null values.
    * Produces application log alert to `log-alert` stream. 
    
    To produce message to `avro-applicationlog` stream, start the `axual-client-avro-producer` app.
    <br/>To produce message to `log-alert-setting` stream, start the `axual-client-avro-log-alert-setting-producer` app.
    <br/>To consume the messages from log-alert` stream, start the `axual-client-avro-log-alert-consumer` app.

* data-definitions\
Contains projects for stream schemas/class definitions:
  * apache-avro-schema-example\
  Contains the Apache Avro schema definitions used by the examples
* example-standalone\
A standalone lightweight Axual Platform configured to host the example streams.
* example-common\
A library containing the settings/configurations shared by the example applications and standalone server
The Server and Client SSL Certificates are also stored here


## Docker Compose
A Docker compose file is available.
It uses the same settings as the _example-common_ project settings.
Make sure you've packaged the _apache-avro-schema-example_ project before running the docker-compose file:

    cd data-definitions/apache-avro-schema-example
    mvn clean package

Then start the compose from the root of the project:

    cd ../.. #(in case you just did the previous step)
    docker-compose up

## License
Axual Java Client Example is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).