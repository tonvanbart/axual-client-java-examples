//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.common;

import org.apache.avro.Schema;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogAlert;
import io.axual.client.example.schema.ApplicationLogAlertSetting;
import io.axual.client.example.schema.ApplicationLogEvent;

public enum Streams {
    AVRO_APPLICATIONLOG("avro-applicationlog", Application.SCHEMA$, ApplicationLogEvent.SCHEMA$, 10, false),
    LOG_ALERT_SETTING("avro-log-alert-setting", Application.SCHEMA$, ApplicationLogAlertSetting.SCHEMA$, 10, false),
    LOG_ALERT_SETTING_CHANGE_LOG(String.join("-", Applications.CLIENT_AVRO_JOIN_STREAM.getApplicationId(), Applications.CLIENT_AVRO_JOIN_STREAM.getStore(), "changelog"), Application.SCHEMA$, ApplicationLogAlertSetting.SCHEMA$, 10, true),
    LOG_ALERT("avro-log-alert", Application.SCHEMA$, ApplicationLogAlert.SCHEMA$, 10, false),
    STRING_APPLICATIONLOG("string-applicationlog", null, null, 10, false);

    Streams(String stream, Schema keySchema, Schema valueSchema, int partitions, boolean enableCompaction) {
        this.stream = stream;
        this.keySchema = keySchema;
        this.valueSchema = valueSchema;
        this.partitions = partitions;
        this.enableCompaction = enableCompaction;
    }

    private String stream;
    private Schema keySchema;
    private Schema valueSchema;
    private int partitions;
    private boolean enableCompaction;

    public String getStream() {
        return stream;
    }

    public Schema getKeySchema() {
        return keySchema;
    }

    public Schema getValueSchema() {
        return valueSchema;
    }

    public int getPartitions() {
        return partitions;
    }

    public boolean isEnableCompaction() {
        return enableCompaction;
    }
}
