//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.common;

public class ServerSettings {
    public static final String TENANT = "axual";
    public static final String INSTANCE = "example";
    public static final String ENVIRONMENT = "local";
    public static final String CLUSTER = "cluster";
    public static final String SYSTEM = "axual-standalone";

    public static final String LISTENADDRESS = "127.0.0.1";
    public static final int ENDPOINTPORT = 8081;
    public static final int SCHEMAREGISTRYPORT = 8082;
    public static final int BROKERPORT = 8083;
    public static final int ZOOKEEPERPORT = 8084;

    public static final String GROUPPATTERN = "{tenant}-{instance}-{environment}-{group}";
    public static final String TOPICPATTERN = "{tenant}-{instance}-{environment}-{topic}";

    public static final String KEYSTORE_RESOURCE_PATH = "axual.server.keystore.jks";
    public static final String KEYSTORE_PASSWORD = "notsecret";
    public static final String KEY_PASSWORD = "notsecret";
    public static final String TRUSTSTORE_RESOURCE_PATH = "axual.server.truststore.jks";
    public static final String TRUSTSTORE_PASSWORD = "notsecret";

    public static String getEndpoint() {
        return String.format("http://%s:%d", LISTENADDRESS, ENDPOINTPORT);
    }
}
