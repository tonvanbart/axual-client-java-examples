package io.axual.client.example.axualclient.string;

import static com.jayway.awaitility.Awaitility.await;

import io.axual.client.AxualClient;
import io.axual.client.config.ConsumerConfig;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.ProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.producer.ProducedMessage;
import io.axual.common.config.ClientConfig;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.jupiter.SingleClusterPlatformUnit;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class LogEventStringProducerTest {

  private static final Logger LOG = LoggerFactory.getLogger(LogEventStringProducerTest.class);

  private static final String STREAM = Streams.STRING_APPLICATIONLOG.getStream();
  private static final int PARTITIONS = Streams.STRING_APPLICATIONLOG.getPartitions();
  private static final String APPLICATION_ID = "io.axual.specific.string.producer.test";
  private static final String APPLICATION_NAME = "Test LogEventStringProducer";
  private static final String APPLICATION_VERSION = "0.0.0";
  private static final String APPLICATION_OWNER = "Test";

  private static final String LOG_MESSAGE_APPLICATION_NAME = "Log App";
  private static final String LOG_MESSAGE_FORMAT = "Message %d";
  private static final String LOG_MESSAGE_CONTEXT_ITERATION_KEY = "iteration";

  private class TestingRecord {

    final int iteration;
    final String applicationName;
    final long timestamp;
    final ApplicationLogLevel logLevel;
    final String logMessage;
    final Map<CharSequence, CharSequence> context;

    public TestingRecord(int iteration, String applicationName, long timestamp,
        ApplicationLogLevel logLevel, String logMessage, Map<CharSequence, CharSequence> context) {
      this.iteration = iteration;
      this.applicationName = applicationName;
      this.timestamp = timestamp;
      this.logLevel = logLevel;
      this.logMessage = logMessage;
      this.context = context;
    }

    @Override
    public String toString() {
      return "TestRecord:" +
          "\titeration       : '" + iteration + "'\n" +
          "\tapplicationName : '" + applicationName + "'\n" +
          "\ttimestamp       : '" + timestamp + "'\n" +
          "\tlogLevel        : '" + logLevel + "'\n" +
          "\tlogMessage      : '" + logMessage + "'\n"
          ;
    }
  }

  @RegisterExtension
  public SingleClusterPlatformUnit platform = new SingleClusterPlatformUnit()
      .addStream(
          new StreamConfig()
              .setName(STREAM)
              .setPartitions(PARTITIONS)
      );

  @Test
  public void produce() {

    // Create the test set
    List<TestingRecord> testingRecords = IntStream.range(0, 10).mapToObj(iteration -> {
      return new TestingRecord(
          iteration,
          LOG_MESSAGE_APPLICATION_NAME
          , 100 + iteration
          , ApplicationLogLevel.FATAL
          , String.format(LOG_MESSAGE_FORMAT, iteration), Collections
          .singletonMap(LOG_MESSAGE_CONTEXT_ITERATION_KEY, Integer.toString(iteration)));
    }).collect(Collectors.toList());

    // Get the clientConfig
    ClientConfig clientConfig = platform.instance().getClientConfig(APPLICATION_ID);

    // Create queue, processors and configs for the verifying consumer
    Queue<ConsumerMessage<String, String>> queue = new LinkedList<>();
    EnqueueProcessor processor = new EnqueueProcessor("ProducerTest Queue", queue, true);
    ConsumerConfig<String, String> consumerConfig = ConsumerConfig.<String, String>builder()
        .setKeyDeserializer(new StringDeserializer())
        .setValueDeserializer(new StringDeserializer())
        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
        .setStream(STREAM)
        .build();

    // Create producer config
    ProducerConfig<String, String> producerConfig = ProducerConfig.<String, String>builder()
        .setKeySerializer(new StringSerializer())
        .setValueSerializer(new StringSerializer())
        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
        .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
        .build();

    try (final AxualClient axualClient = new AxualClient(clientConfig);
        final LogEventStringProducer producer = new LogEventStringProducer(axualClient,
            producerConfig, APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_OWNER);
        final Consumer<String, String> consumer = axualClient
            .buildConsumer(consumerConfig, processor)) {
      Future<ConsumeFailedException> consumerFuture = consumer.startConsuming();
      List<Future<ProducedMessage<String, String>>> produceFutures =
          testingRecords.stream().map(record -> producer.produce(
              record.applicationName,
              record.timestamp,
              record.logLevel,
              record.logMessage,
              record.context)
          ).collect(Collectors.toList());

      LOG.info("Waiting for all producer messages to be produced");
      await("Check producer")
          .atMost(60, TimeUnit.SECONDS)
          .pollDelay(1, TimeUnit.SECONDS)
          .pollInterval(500, TimeUnit.MILLISECONDS)
          .until(() -> {
            produceFutures.removeIf(future -> {
              if (future.isDone()) {
                try {
                  ProducedMessage<String, String> message = future.get();
                  LOG.info("Produced message at {} on \n" +
                          "stream    : {}\n" +
                          "partition : {}\n" +
                          "offset    : {}\n" +
                          "cluster   : {}\n" +
                          "system    : {}\n" +
                          "instance  : {}\n"
                      , message.getTimestamp()
                      , message.getStream()
                      , message.getPartition()
                      , message.getOffset()
                      , message.getCluster()
                      , message.getSystem()
                      , message.getInstance()
                  );
                  return true;
                } catch (InterruptedException | ExecutionException e) {
                  e.printStackTrace();
                }
              }
              return false;
            });
            return produceFutures.isEmpty();
          });

      LOG.info("Waiting for all Consumer messages to be read");
      await("Checking nr of consumer messages")
          .atMost(15, TimeUnit.SECONDS)
          .pollDelay(1, TimeUnit.SECONDS)
          .pollInterval(500, TimeUnit.MILLISECONDS)
          .until(() -> queue.size() >= testingRecords.size());

      final ConsumeFailedException consumeFailedExceptionFromStop = consumer.stopConsuming();
      if (consumeFailedExceptionFromStop != null) {
        LOG.warn("Consume failed from stop", consumeFailedExceptionFromStop);
      }

      await("Wait for consumer to be fully stopped")
          .atMost(15, TimeUnit.SECONDS)
          .pollDelay(1, TimeUnit.SECONDS)
          .pollInterval(500, TimeUnit.MILLISECONDS)
          .until(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
              return consumerFuture.isDone();
            }
          });
      try {
        if (consumerFuture.isCancelled()) {
          LOG.warn("Consumer was cancelled");
        } else {
          final ConsumeFailedException consumeFailedExceptionFromFuture = consumerFuture.get();
          if (consumeFailedExceptionFromFuture != null) {
            LOG.warn("Consume failed from future", consumeFailedExceptionFromFuture);
          }
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    }

  }
}