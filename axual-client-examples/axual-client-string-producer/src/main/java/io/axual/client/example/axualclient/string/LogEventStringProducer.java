//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.string;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.Future;

import io.axual.client.AxualClient;
import io.axual.client.config.ProducerConfig;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;

public class LogEventStringProducer implements AutoCloseable {
    public static final Logger LOG = LoggerFactory.getLogger(LogEventStringProducer.class);
    private final Producer<String, String> producer;
    private final Application application;

    public LogEventStringProducer(
            final AxualClient axualClient
            , final ProducerConfig<String, String> producerConfig
            , final String applicationName
            , final String applicationVersion
            , final String applicationOwner) {
        producer = axualClient.buildProducer(producerConfig);
        this.application = Application.newBuilder()
                .setName(applicationName)
                .setVersion(applicationVersion)
                .setOwner(applicationOwner)
                .build();
    }

    public Future<ProducedMessage<String, String>> produce(
            final String applicationName,
            final long timestamp,
            final ApplicationLogLevel logLevel,
            final String logMessage,
            final Map<CharSequence, CharSequence> context) {
        Application key = Application.newBuilder()
                .setName(applicationName)
                .setVersion("1.9.9")
                .setOwner("none")
                .build();
        ApplicationLogEvent value = ApplicationLogEvent.newBuilder()
                .setTimestamp(timestamp)
                .setSource(this.application)
                .setLevel(logLevel)
                .setMessage(logMessage)
                .setContext(context)
                .build();

        LOG.info("key = '{}', value = '{}'", key.toString(), value.toString());

        ProducerMessage<String, String> message = ProducerMessage.<String, String>newBuilder()
                .setStream(Streams.STRING_APPLICATIONLOG.getStream())
//                .setKey(key.toString())
                .setKey("key")
//                .setValue(value.toString())
                .setValue("this is the value")
                .build();
        Future<ProducedMessage<String, String>> result = producer.produce(message);
        return result;
    }

    @Override
    public void close() {
        producer.close();
    }
}
