package io.axual.client.example.spring;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "axual.client.settings")
public class AxualClientSettings {

    private String tenant = "axual";
    private String instance = "example";
    private String environment = "local";
    private String cluster = "cluster";
    private String system = "axual-standalone";

    private String listenaddress = "127.0.0.1";
    private int endpointport = 8081;
    private int schemaregistryport = 8082;
    private int brokerport = 8083;
    private int zookeeperport = 8084;

    private String grouppattern = "{tenant}-{instance}-{environment}-{group}";
    private String topicpattern = "{tenant}-{instance}-{environment}-{topic}";

    private String keystoreLocation = "axual.server.keystore.jks";
    private String keystorePassword = "notsecret";
    private String keyPassword = "notsecret";
    private String truststoreLocation = "axual.server.truststore.jks";
    private String truststorePassword = "notsecret";

    private String applicationId = "io.axual.example.client.string.producer";
    private String applicationVersion = "0.0.1";

    public String getEndpoint() {
        return String.format("http://%s:%d", listenaddress, endpointport);
    }


}
