package io.axual.client.example.spring;

import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.ProducerConfig;
import io.axual.client.producer.Producer;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

import java.net.URL;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;

@SpringBootApplication
@EnableConfigurationProperties(AxualClientSettings.class)
public class SpringExampleApplication {

    private  final String KEYSTORE_LOCATION = getResourceFilePath("axual.client.keystore.jks");
    private static final PasswordConfig KEYSTORE_PASSWORD = new PasswordConfig("notsecret");
    private static final PasswordConfig KEY_PASSWORD = new PasswordConfig("notsecret");
    private  final String TRUSTSTORE_LOCATION = getResourceFilePath("axual.client.keystore.jks");
    private static final PasswordConfig TRUSTSTORE_PASSWORD = new PasswordConfig("notsecret");

    private static final Logger log = LoggerFactory.getLogger(SpringExampleApplication.class);

    @Bean
    public ClientConfig clientConfig(AxualClientSettings axualSettings) {
        log.info("clientConfig({})", axualSettings);
        return ClientConfig.newBuilder()
                .setApplicationId(axualSettings.getApplicationId())
                .setApplicationVersion(axualSettings.getApplicationVersion())
                .setEndpoint(axualSettings.getEndpoint())
                .setTenant(axualSettings.getTenant())
                .setEnvironment(axualSettings.getEnvironment())
                .setSslConfig(
                        SslConfig.newBuilder()
                                .setEnableHostnameVerification(false)
                                .setKeystoreLocation(getResourceFilePath(axualSettings.getKeystoreLocation()))
                                .setKeystorePassword(new PasswordConfig(axualSettings.getKeystorePassword()))
                                .setKeyPassword(new PasswordConfig(axualSettings.getKeyPassword()))
                                .setTruststoreLocation(axualSettings.getTruststoreLocation())
                                .setTruststorePassword(new PasswordConfig(axualSettings.getTruststorePassword()))
                                .build()
                )
                .build();
    }

    @Bean
    public ProducerConfig producerConfig(ClientConfig clientConfig) {
        log.info("producerConfig({})", clientConfig);
        return ProducerConfig.<String, String>builder()
                        .setKeySerializer(new StringSerializer())
                        .setValueSerializer(new StringSerializer())
                        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                        .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                        .setMessageBufferWaitTimeout(100)
                        .setBatchSize(1)
                        .setProxyChain(ProxyChain.newBuilder()
                                .append(SWITCHING_PROXY_ID)
                                .append(RESOLVING_PROXY_ID)
                                .append(LINEAGE_PROXY_ID)
                                .append(HEADER_PROXY_ID)
                                .build())
                        .build();
    }

    @Bean
    public AxualClient axualClient(ClientConfig clientConfig) {
        log.info("axualClient({})", clientConfig);
        return new AxualClient(clientConfig);
    }

    @Bean
    public Producer producer(AxualClient axualClient, ProducerConfig producerConfig) {
        log.info("producer({},{})", axualClient, producerConfig);
        return axualClient.buildProducer(producerConfig);
    }

	public static void main(String[] args) {
		SpringApplication.run(SpringExampleApplication.class, args);
	}

    private String getResourceFilePath(final String resource) {
        log.info("getResourceFilePath({})", resource);
        URL resourceUrl = ClassLoader.getSystemClassLoader().getResource(resource);
        log.info("===> resourceUrl={}", resourceUrl);
        return resourceUrl.getFile();
    }

}
