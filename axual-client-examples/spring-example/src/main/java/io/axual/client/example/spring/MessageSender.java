package io.axual.client.example.spring;

import io.axual.client.AxualClient;
import io.axual.client.config.ProducerConfig;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.common.config.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
public class MessageSender implements CommandLineRunner {

    @Autowired
    private ClientConfig clientConfig;

    @Autowired
    private ProducerConfig producerConfig;

    @Autowired
    private AxualClient axualClient;

    @Autowired
    private Producer producer;

    private static final Logger log = LoggerFactory.getLogger(MessageSender.class);

    @Override
    public void run(String... strings) throws Exception {
        log.info("clientConfig={}", clientConfig);
        log.info("producerConfig={}", producerConfig);
        log.info("axualClient={}", axualClient);
        log.info("run()");
        IntStream.range(1,11).forEach(i -> {
            log.info("Sending message {}", i);
            ProducerMessage producerMessage = ProducerMessage.newBuilder()
                .setStream("string-applicationlog")
                .setKey("key" + i)
                .setValue("this is the value of message " + i)
                .build();
            producer.produce(producerMessage);
        });
        producer.close();
    }
}
