//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclientproxy.avro;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.config.types.Password;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.axual.client.example.common.Applications;
import io.axual.client.example.common.ServerSettings;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.CommonConfig;
import io.axual.serde.avro.SpecificAvroSerializer;

import static io.axual.client.proxy.axual.producer.AxualProducerConfig.CHAIN_CONFIG;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static java.lang.Thread.sleep;

public class ProxyAvroProducerApp {
    private static final Logger LOG = LoggerFactory.getLogger(ProxyAvroProducerApp.class);
    private static final String APPLICATION_ID = Applications.PROXY_AVRO_CONSUMER.getApplicationId();
    private static final String APPLICATION_VERSION = Applications.PROXY_AVRO_CONSUMER.getApplicationVersion();
    private static final String ENDPOINT = ServerSettings.getEndpoint();
    private static final String TENANT = ServerSettings.TENANT;
    private static final String ENVIRONMENT = Applications.PROXY_AVRO_CONSUMER.getEnvironment();

    private static final String KEYSTORE_LOCATION = getResourceFilePath(Applications.PROXY_AVRO_CONSUMER.getKeystoreLocation());
    private static final Password KEYSTORE_PASSWORD = new Password(Applications.PROXY_AVRO_CONSUMER.getKeystorePassword());
    private static final Password KEY_PASSWORD = new Password(Applications.PROXY_AVRO_CONSUMER.getKeyPassword());
    private static final String TRUSTSTORE_LOCATION = getResourceFilePath(Applications.PROXY_AVRO_CONSUMER.getKeystoreLocation());
    private static final Password TRUSTSTORE_PASSWORD = new Password(Applications.PROXY_AVRO_CONSUMER.getTruststorePassword());

    private static final String APPLICATION_NAME = "Axual Proxy Specific Avro Producer";
    private static final String APPLICATION_OWNER = "Team Log";

    public static void main(String[] args) throws InterruptedException {
        LOG.info("Creating producer config map");
        Map<String, Object> config = new HashMap<>();
        config.put(CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(SWITCHING_PROXY_ID)
                .append(RESOLVING_PROXY_ID)
                .append(LINEAGE_PROXY_ID)
                .append(HEADER_PROXY_ID)
                .build());
        config.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        config.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);
        config.put(CommonConfig.TENANT, TENANT);
        config.put(CommonConfig.ENVIRONMENT, ENVIRONMENT);

        config.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, ENDPOINT);
        config.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);
        config.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, KEYSTORE_LOCATION);
        config.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, KEYSTORE_PASSWORD);
        config.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, KEY_PASSWORD);
        config.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, TRUSTSTORE_LOCATION);
        config.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, TRUSTSTORE_PASSWORD);

        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, new SpecificAvroSerializer<Application>());
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, new SpecificAvroSerializer<ApplicationLogEvent>());
        config.put(ProducerConfig.ACKS_CONFIG, "-1");
        config.put(ProducerConfig.RETRIES_CONFIG, "0");
        config.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, "1000");
        config.put(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG, "1000");
        config.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");

        List<Future<RecordMetadata>> futures;

        LOG.info("Creating client and LogSpecificEventProducer");
        try (final LogEventSpecificProducer producer = new LogEventSpecificProducer(config, APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_OWNER)) {
            // Produce 10 messages
            futures = IntStream.range(0, 10).mapToObj(i -> producer.produce(APPLICATION_NAME, 1000 + i, ApplicationLogLevel.INFO, String.format("Message %d", i), Collections.singletonMap("Some key", "Some Value"))).collect(Collectors.toList());
        }

        do {
            futures.removeIf(future -> {
                if (!future.isDone()) {
                    return false;
                }

                try {
                    RecordMetadata producedMessage = future.get();
                    LOG.info("Produced message to partition {} offset {}", producedMessage.partition(), producedMessage.offset());
                } catch (InterruptedException | ExecutionException e) {
                    LOG.error("Error getting future, produce failed", e);
                }
                return true;
            });
            sleep(100);
        } while (!futures.isEmpty());

        LOG.info("Done!");
    }

    private static String getResourceFilePath(final String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }
}
