//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.avro;

import org.apache.kafka.common.config.types.Password;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.example.common.Applications;
import io.axual.client.example.common.ServerSettings;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.CommonConfig;
import io.axual.serde.avro.SpecificAvroDeserializer;

import static io.axual.client.proxy.axual.consumer.AxualConsumerConfig.CHAIN_CONFIG;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static java.lang.Thread.sleep;
import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.CommonClientConfigs.SECURITY_PROTOCOL_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.*;
import static org.apache.kafka.common.config.SslConfigs.*;

public class ProxyAvroConsumerApp {
    private static final Logger LOG = LoggerFactory.getLogger(ProxyAvroConsumerApp.class);
    private static final String APPLICATION_ID = Applications.PROXY_AVRO_CONSUMER.getApplicationId();
    private static final String APPLICATION_VERSION = Applications.PROXY_AVRO_CONSUMER.getApplicationVersion();
    private static final String ENDPOINT = ServerSettings.getEndpoint();
    private static final String TENANT = ServerSettings.TENANT;
    private static final String ENVIRONMENT = Applications.PROXY_AVRO_CONSUMER.getEnvironment();

    private static final String KEYSTORE_LOCATION = getResourceFilePath(Applications.PROXY_AVRO_CONSUMER.getKeystoreLocation());
    private static final Password KEYSTORE_PASSWORD = new Password(Applications.PROXY_AVRO_CONSUMER.getKeystorePassword());
    private static final Password KEY_PASSWORD = new Password(Applications.PROXY_AVRO_CONSUMER.getKeyPassword());
    private static final String TRUSTSTORE_LOCATION = getResourceFilePath(Applications.PROXY_AVRO_CONSUMER.getKeystoreLocation());
    private static final Password TRUSTSTORE_PASSWORD = new Password(Applications.PROXY_AVRO_CONSUMER.getTruststorePassword());

    public static void main(String[] args) throws InterruptedException {
        LOG.info("Creating producer config map");
        Map<String, Object> config = new HashMap<>();

        config.put(CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(SWITCHING_PROXY_ID)
                .append(RESOLVING_PROXY_ID)
                .append(LINEAGE_PROXY_ID)
                .append(HEADER_PROXY_ID)
                .build());
        config.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        config.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);
        config.put(CommonConfig.TENANT, TENANT);
        config.put(CommonConfig.ENVIRONMENT, ENVIRONMENT);

        config.put(BOOTSTRAP_SERVERS_CONFIG, ENDPOINT);
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);
        config.put(SSL_KEYSTORE_LOCATION_CONFIG, KEYSTORE_LOCATION);
        config.put(SSL_KEYSTORE_PASSWORD_CONFIG, KEYSTORE_PASSWORD);
        config.put(SSL_KEY_PASSWORD_CONFIG, KEY_PASSWORD);
        config.put(SSL_TRUSTSTORE_LOCATION_CONFIG, TRUSTSTORE_LOCATION);
        config.put(SSL_TRUSTSTORE_PASSWORD_CONFIG, TRUSTSTORE_PASSWORD);

        config.put(KEY_DESERIALIZER_CLASS_CONFIG, new SpecificAvroDeserializer<>());
        config.put(VALUE_DESERIALIZER_CLASS_CONFIG, new SpecificAvroDeserializer<ApplicationLogEvent>());
        config.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(MAX_POLL_RECORDS_CONFIG, 1);
        config.put(ENABLE_AUTO_COMMIT_CONFIG, "false");

        try (LogEventSpecificConsumer consumer = new LogEventSpecificConsumer(config, Streams.AVRO_APPLICATIONLOG.getStream())) {
            while (consumer.isConsuming()) {
                sleep(100);
            }
        }
    }

    private static String getResourceFilePath(final String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }
}
