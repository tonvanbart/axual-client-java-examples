//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.examples.axualclient.stream;

import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.config.DeliveryStrategy;
import io.axual.client.example.common.Applications;
import io.axual.client.example.common.ServerSettings;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogAlert;
import io.axual.client.example.schema.ApplicationLogAlertSetting;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.serde.avro.SpecificAvroSerde;
import io.axual.streams.AxualStreamsClient;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.StreamRunner;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;

public class ClientStreamJoinApp {
    private static final Logger LOG = LoggerFactory.getLogger(ClientStreamJoinApp.class);
    private static final String APPLICATION_ID = Applications.CLIENT_AVRO_JOIN_STREAM.getApplicationId();
    private static final String APPLICATION_VERSION = Applications.CLIENT_AVRO_JOIN_STREAM.getApplicationVersion();
    private static final String ENDPOINT = ServerSettings.getEndpoint();
    private static final String TENANT = ServerSettings.TENANT;
    private static final String ENVIRONMENT = Applications.CLIENT_AVRO_JOIN_STREAM.getEnvironment();
    private static final String STORE = Applications.CLIENT_AVRO_JOIN_STREAM.getStore();

    private static final String KEYSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_AVRO_JOIN_STREAM.getKeystoreLocation());
    private static final PasswordConfig KEYSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_JOIN_STREAM.getKeystorePassword());
    private static final PasswordConfig KEY_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_JOIN_STREAM.getKeyPassword());
    private static final String TRUSTSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_AVRO_JOIN_STREAM.getTruststoreLocation());
    private static final PasswordConfig TRUSTSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_JOIN_STREAM.getTruststorePassword());

    private static final String APPLICATION_NAME = "Axual Client Stream App";
    private static final String APPLICATION_OWNER = "Team Log";

    public static void main(String[] args) {
        LOG.info("Creating ClientConfig...");
        ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setTenant(TENANT)
                .setEnvironment(ENVIRONMENT)
                .setSslConfig(
                        SslConfig.newBuilder()
                                .setEnableHostnameVerification(false)
                                .setKeystoreLocation(KEYSTORE_LOCATION)
                                .setKeystorePassword(KEYSTORE_PASSWORD)
                                .setKeyPassword(KEY_PASSWORD)
                                .setTruststoreLocation(TRUSTSTORE_LOCATION)
                                .setTruststorePassword(TRUSTSTORE_PASSWORD)
                                .build()
                )
                .build();

        LOG.info("Creating Topology...");
        /*
         * Steps:
         * 1- Read stream from avro-applicationlog.
         * 2- Join by table source by stream log-alert-setting.
         * 3- Filter all Null joins (no entry in stream log-alert-setting).
         * 4- Write to log-alert.
         *
         * Note: Before using the join make sure to create intermediate stream with name as
         * {applicationId}_{storeName}_changelog
         *
         * The example-standalone project creates this intermediate stream, as it already mentioned in the Streams enum (under example-common project).
         * */
        final TopologyFactory topology = builder -> {
            KStream<Application, ApplicationLogEvent> sourceStream = builder.stream(Streams.AVRO_APPLICATIONLOG.getStream());
            KTable<Application, ApplicationLogAlertSetting> sourceTable = builder.table(Streams.LOG_ALERT_SETTING.getStream(), Materialized.as(STORE));
            sourceStream
                    .peek((k, v) -> LOG.info("Key: {} Value: {}", k, v))
                    .leftJoin(sourceTable, ClientStreamJoinApp::generateApplicationLogAlert)
                    .filterNot((k, v) -> v == null)
                    .to(Streams.LOG_ALERT.getStream());
            return builder.build();
        };

        LOG.info("Creating StreamRunnerConfig...");
        // Prepare streamRunner
        final StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setDefaultKeySerde(SpecificAvroSerde.class.getName())
                .setDefaultValueSerde(SpecificAvroSerde.class.getName())
                .setProxyChain(ProxyChain.newBuilder()
                        .append(SWITCHING_PROXY_ID)
                        .append(RESOLVING_PROXY_ID)
                        .append(LINEAGE_PROXY_ID)
                        .append(HEADER_PROXY_ID)
                        .build())
                .setTopologyFactory(topology)
                .build();

        LOG.info("Creating AxualStreamsClient and StreamRunner...");
        AxualStreamsClient streamsClient = new AxualStreamsClient(config);
        StreamRunner streamRunner = streamsClient.buildStreamRunner(streamRunnerConfig);
        streamRunner.start();
    }

    private static ApplicationLogAlert generateApplicationLogAlert(ApplicationLogEvent appLogEvent, ApplicationLogAlertSetting appLogAlertSetting) {
        return appLogAlertSetting == null ? null : ApplicationLogAlert.newBuilder()
                .setTimestamp(System.currentTimeMillis())
                .setSource(Application.newBuilder()
                        .setName(APPLICATION_NAME)
                        .setVersion(APPLICATION_VERSION)
                        .setOwner(APPLICATION_OWNER)
                        .build())
                .setLogEvent(appLogEvent)
                .setSetting(appLogAlertSetting)
                .setTarget(APPLICATION_NAME)
                .build();
    }

    private static String getResourceFilePath(final String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }
}
